module Circle (
Circle (..),
getArea,
getPerimeter
) where 
import Point as Point


data Circle = Circle { punto:: Point, radium :: Float } deriving (Show)

getArea :: Circle -> Float
getArea (Circle _ r) = pi * r ^ 2

getPerimeter :: Circle -> Float
getPerimeter (Circle _ r) = 2 * (pi * r)  
