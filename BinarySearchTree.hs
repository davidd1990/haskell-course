module BinarySearchTree(
Tree(..),
singleton
) where 

data Tree a = EmptyTree | Node a (Tree a) (Tree a) deriving (Show, Read, Eq)  

singleton :: a -> Tree a  
singleton x = Node x EmptyTree EmptyTree  
  
treeInsert :: (Ord a) => a -> Tree a -> Tree a  
treeInsert x EmptyTree = singleton x  
treeInsert x (Node a left right)   
    | x == a = Node x left right  
    | x < a  = Node a (treeInsert x left) right  
    | x > a  = Node a left (treeInsert x right)  
	
treeElem :: (Ord a) => a -> Tree a -> Bool  
treeElem x EmptyTree = False  
treeElem x (Node a left right)  
    | x == a = True  
    | x < a  = treeElem x left  
    | x > a  = treeElem x right  

preorder ::  Tree a -> [a]
preorder  EmptyTree = []
preorder (Node a left right) =
		let l = a : preorder left
		    r =  preorder right
		in l ++ r

inorder ::  Tree a -> [a]
inorder  EmptyTree = []
inorder (Node a left right) =
		let l =  inorder left
		    r =  a :inorder right
		in l ++ r
		
postorder ::  Tree a -> [a]
postorder  EmptyTree =  []
postorder (Node a left right) =
		let l =  postorder left
		    r = postorder right
		in l  ++ r ++ [a]
