removeNonEqual :: (Eq a) => a -> [a] -> [a]
removeNonEqual _ [] = []
removeNonEqual a (x:xs)
 | a == x  = x : removeNonEqual a xs
 | otherwise = removeNonEqual a xs

isPrime :: Int -> Bool
isPrime 1 = False
isPrime 2 = True
isPrime n 
 | (length [x | x <- [2 .. n-1],mod n x == 0]) > 0 = False
 | otherwise = True
 
countOdd :: [Int] -> Int
countOdd  ys = foldl (\acc x -> if x `mod` 2 > 0  then acc + 1  else acc ) 0 ys

nLastEven :: Int -> [Int] -> [Int]
nLastEven x xs =  b where
  a = length xs
  c = a - x
  b = [ a | a <- drop c xs,  even a]

countPrime ::[Int] -> [Int]
countPrime xs = [x | x <- xs,isPrime x]
