functionOne :: Int -> [a] -> [a] -> ([a],[a])
functionOne i x y  = (b, a) where
   a = c ++ e
   b = d ++ f
   c = take i x
   e = drop i y
   d = take i y
   f = drop i x

functionTwo :: String -> Int
functionTwo y = r where
  a = words y
  b = [ c | c  <- a, isNumber c]
  d = [ read x :: Int | x <- b]
  r = sum d

isNumber :: String -> Bool
isNumber str =
    case (reads str) :: [(Double, String)] of
      [(_, "")] -> True
      _         -> False                           
