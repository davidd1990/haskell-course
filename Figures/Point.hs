module Point (
getX,
getY,
Point (..)
) where

data Point = Point { x :: Float, y :: Float} deriving (Show)

getX :: Int -> Int
getX x = x

getY :: Int -> Int
getY x = x
