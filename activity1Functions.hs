factorial :: (Integral a) => a -> a
factorial 0 = 1
factorial 1 = 1
factorial n = n * factorial (n - 2)

repeatit :: Int -> a -> [a]
repeatit x a = take x (repeat a)

naturalPower :: (Num a, Integral b) => a -> b -> a
naturalPower x y = x ^ y

linking :: [[a]] -> [a]
linking xxs = [ y | x <- xxs, y <- x]

getElement :: [a] -> Int -> a
getElement x y = x!!y

