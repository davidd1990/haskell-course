module PrincipalDataStructure (
insertNewElementLinkedList,
removeElementLinkedList,
searchElementLinkedList
) where

import OrderedLinkedList as O

insertNewElementLinkedList :: (Ord a) => a -> LinkedList a -> LinkedList a
insertNewElementLinkedList a lista = O.ingresarElemtento a lista

removeElementLinkedList :: (Ord a) => a -> LinkedList a -> LinkedList a
removeElementLinkedList a lista = O.borrarElemento a lista

searchElementLinkedList :: (Ord a) => a -> LinkedList a -> Bool
searchElementLinkedList a lista = O.searchElement a lista