module OrderedLinkedList (
LinkedList (..),
searchElement,
ingresarElemtento,
crearLinkedList,
borrarElemento
) where

data LinkedList a = EmptyList | Node a (LinkedList a) deriving (Show, Read, Eq)


singleton :: a -> LinkedList a
singleton x = Node x EmptyList

insertElement :: (Ord a) => a -> LinkedList a -> LinkedList a
insertElement x EmptyList = singleton x
insertElement x (Node a  lista) = Node a (insertElement x lista)

ingresarElemtento :: (Ord a) => a -> LinkedList a -> LinkedList a
ingresarElemtento a  lista = Node a lista


searchElement :: (Ord a) => a -> LinkedList a -> Bool
searchElement x EmptyList = False
searchElement x (Node a  lista)
    | x == a = True
    | otherwise = searchElement x lista

crearLinkedList :: (Ord a) => [a] -> LinkedList a
crearLinkedList a = foldr insertElement EmptyList a


borrarElementoLista :: (Ord a) => a -> LinkedList a -> [a]
borrarElementoLista x EmptyList = []
borrarElementoLista  x (Node a  lista)
	| x == a = borrarElementoLista x lista
    | otherwise =  a : borrarElementoLista x lista

borrarElemento :: (Ord a) => a -> LinkedList a -> LinkedList a
borrarElemento x lista = w where
	a = borrarElementoLista x lista
	b = reverse a
	w = crearLinkedList b