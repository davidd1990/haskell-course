module Rectangle (
getArea,
getPerimeter,
Rectangle(..)
) where 
import Point as Point 

data Rectangle = Rectangle { punto1 :: Point, punto2 :: Point } deriving (Show)

getArea :: Rectangle -> Float 
getArea (Rectangle (Point x1 y1) (Point x2 y2)) = (abs $ x2 -x1) * (abs $ y2 -y1)

getPerimeter :: Rectangle -> Float
getPerimeter (Rectangle (Point x1 y1) (Point x2 y2)) = (2 * (abs $ x2 -x1)) + (2 * (abs $ y2- y1))
