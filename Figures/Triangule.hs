module Triangule (
Triangule (..),
getArea,
getPerimeter
) where 

import Point 

data Triangule = Triangule { punto1 :: Point, punto2 :: Point, punto3 :: Point} deriving (Show)

getArea :: Triangule -> Float
getArea (Triangule (Point x1 y1) (Point x2 y2) (Point x3 y3)) = w where 
		a = sqrt (((abs $ x2 - x1)^ 2) + ((abs $ y2 - y1)^ 2))
		b = sqrt (((abs $ x3 - x1)^ 2) + ((abs $ y3 - y1)^ 2))
		c = sqrt (((abs $ x3 - x2)^ 2) + ((abs $ y3 - y2)^ 2))
		s = (a+b+c)/2
		w =  sqrt (s*((s-a)*(s-b)*(s-c)))
  

getPerimeter :: Triangule -> Float
getPerimeter (Triangule (Point x1 y1) (Point x2 y2) (Point x3 y3)) = s where
                a = sqrt (((abs $ x2 - x1)^ 2) + ((abs $ y2 - y1)^ 2))
                b = sqrt (((abs $ x3 - x1)^ 2) + ((abs $ y3 - y1)^ 2))
                c = sqrt (((abs $ x3 - x2)^ 2) + ((abs $ y3 - y2)^ 2))
                s = (a+b+c)

