module MainApp (
getCircleArea,
getCirclePerimeter,
getRectangleArea,
getRectanglePerimeter,
getTrianguleArea,
getTriangulePerimeter
) where 
import Point 
import qualified  Circle as C
import qualified Rectangle as R
import qualified Triangule as T


getTrianguleArea :: T.Triangule -> Float
getTrianguleArea x = T.getArea x

getTriangulePerimeter :: T.Triangule -> Float
getTriangulePerimeter x = T.getPerimeter x




getRectangleArea :: R.Rectangle -> Float
getRectangleArea x = R.getArea x

getRectanglePerimeter :: R.Rectangle -> Float
getRectanglePerimeter x = R.getPerimeter x


getCircleArea :: C.Circle -> Float
getCircleArea x = C.getArea x 

getCirclePerimeter :: C.Circle -> Float
getCirclePerimeter x = C.getPerimeter x

